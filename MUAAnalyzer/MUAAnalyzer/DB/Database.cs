﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUAAnalyzer
{
    public class Database : IDomainContext, IDisposable
    {
        private string providerName;
        private string connString;
        private DbProviderFactory provider;
        private DbConnection connection;
        private DbTransaction transaction;

        /// <summary>
        /// Initializes a new instance of the <see cref="OracleDatabase"/> class.
        /// </summary>
        /// 

        public Database(string providerName, string connString)
        {
            this.providerName = providerName;
            this.connString = connString;
            provider = DbProviderFactories.GetFactory(this.providerName);
        }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        public DbConnection Connection
        {
            get
            {
                EnsureConnection();

                return connection;
            }
        }


        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <value>
        /// The transaction.
        /// </value>
        public DbTransaction Transaction
        {
            get
            {
                if (transaction == null)
                {
                    EnsureConnection();

                    transaction = connection.BeginTransaction();
                }

                return transaction;
            }
        }

        /// <summary>
        /// Ensures the connection.
        /// </summary>
        private void EnsureConnection()
        {
            if (connection != null && connection.State == ConnectionState.Broken)
                connection.Close();

            if (connection == null || connection.State == ConnectionState.Closed)
            {
                connection = provider.CreateConnection();
                connection.ConnectionString = connString;
                connection.Open();
            }
        }


        /// <summary>
        /// Commits the changes.
        /// </summary>
        public void CommitChanges()
        {
            if (transaction != null)
            {
                transaction.Commit();
                transaction = null;
            }
        }

        /// <summary>
        /// Rollbacks the changes.
        /// </summary>
        public void RollbackChanges()
        {
            if (transaction != null)
            {
                transaction.Rollback();
                transaction = null;
            }
        }

        public void Dispose()
        {
            if (connection != null && connection.State != ConnectionState.Closed)
                connection.Close();

            transaction = null;
            connection = null;
        }


        public IEnumerable<object> Query(string query)
        {
            var result = SqlMapper.Query(this.Connection, query);

            if (result != null && result.Count() > 0)
                return result;

            return new List<object>();
        }
 
    }
}
