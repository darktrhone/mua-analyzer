﻿using System.Collections.Generic;

namespace MUAAnalyzer
{
    public interface IDomainContext
    {
        void CommitChanges();
        void RollbackChanges();
        IEnumerable<object> Query(string query);
    }
}
