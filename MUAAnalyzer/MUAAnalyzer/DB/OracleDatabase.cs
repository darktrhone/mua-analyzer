﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUAAnalyzer
{
    public class OracleDatabase : Database
    {
        public OracleDatabase() : base("oracle.manageddataaccess.client", ConfigurationManager.ConnectionStrings["OI.FFM.MUA"].ConnectionString)
        {

        }
    }
}
