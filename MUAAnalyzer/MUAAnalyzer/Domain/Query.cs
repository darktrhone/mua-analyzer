﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MUAAnalyzer.Domain
{
    internal class Query
    {
        string name;

        string query;

        DateTime lastExecutionDate;

        DateTime nextExecutionDate;

        bool isScheduled;

        TimeSpan executionInterval;

        List<Parameter> parameters;

        List<Report> reports;
    }
}
