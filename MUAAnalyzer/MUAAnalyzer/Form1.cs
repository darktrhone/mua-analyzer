﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace MUAAnalyzer
{
    public partial class Form1 : Form
    {
        IDomainContext dbContext;
        public Form1(IDomainContext dbContext)
        {
            this.dbContext = dbContext;
            InitializeComponent();
        }

        private void btnRun_Click(object sender, EventArgs e)
        {

            var result = dbContext.Query(tbQuery.Text);

            DataTable dataTable = ConvertToDataTable(result);

            gridResults.DataSource = dataTable;

            saveOutput(dataTable);

        }

        public DataTable ConvertToDataTable(IEnumerable<dynamic> items)
        {
            var t = new DataTable();
            var first = (IDictionary<string, object>)items.First();
            foreach (var k in first.Keys)
            {
                var c = t.Columns.Add(k);
                var val = first[k];
                if (val != null) c.DataType = val.GetType();
            }

            foreach (var item in items)
            {
                var r = t.NewRow();
                var i = (IDictionary<string, object>)item;
                foreach (var k in i.Keys)
                {
                    var val = i[k];
                    if (val == null) val = DBNull.Value;
                    r[k] = val;
                }
                t.Rows.Add(r);
            }
            return t;



        }

        private void tbQuery_TextChanged(object sender, EventArgs e)
        {

        }

        private void saveOutput(DataTable result)
        {
            string pathString = tbSaveOutputPath.Text;

            if (rbSaveOutputOn.Checked && !string.IsNullOrEmpty(pathString) && Directory.Exists(pathString))
            {
                StringBuilder sb = new StringBuilder();

                IEnumerable<string> columnNames = result.Columns.Cast<DataColumn>().Select(column => column.ColumnName);

                sb.AppendLine(string.Join("|", columnNames));

                foreach (DataRow row in result.Rows)
                {
                    IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                    sb.AppendLine(string.Join("|", fields));
                }

                string fileName = string.Format("{0}_{1}.csv", cbQuery.Text, DateTime.Now.ToString("yyyyMMdd_hhmmss"));

                File.WriteAllText(Path.Combine(pathString, fileName), sb.ToString());

            }
        }
    }
}
