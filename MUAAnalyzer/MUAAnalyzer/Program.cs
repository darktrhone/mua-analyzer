﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleInjector;
using System.Configuration;
using SimpleInjector.Lifestyles;
using SimpleInjector.Diagnostics;

namespace MUAAnalyzer
{

    static class Program
    {
        private static Container container;


        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SetupDependencyInjection();
            Application.Run(container.GetInstance<Form1>());
        }

        static void SetupDependencyInjection()
        {
            container = new Container();
            container.Register<IDomainContext, OracleDatabase>(Lifestyle.Singleton);
        }
    }
}
